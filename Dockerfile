FROM node:16

WORKDIR  /app

RUN apt update && apt install -y chromium

COPY package.json .
COPY package-lock.json .
RUN npm ci

COPY index.js .

ENV JOB_NAME "Speedtest"
ENV GATEWAY_ENDPOINT "localhost"

ENTRYPOINT [ "npm", "start" ]