const { FastAPI, SpeedUnits } = require('fast-api-speedtest');
const client = require('prom-client');

const registry = new client.Registry();
let gateway = new client.Pushgateway(process.env['GATEWAY_ENDPOINT'], [], registry)
const JOBNAME = process.env['JOB_NAME'];


const FastTest = new FastAPI({
    measureUpload: true,
    downloadUnit: SpeedUnits.bps,
    uploadUnit: SpeedUnits.bps
});

FastTest.runTest().then(result => {
    console.log(`Ping: ${result.ping} ms`);
    console.log(`Download speed: ${result.downloadSpeed} ${result.downloadUnit}`);
    console.log(`Upload speed: ${result.uploadSpeed} ${result.uploadUnit}`);

    const ping = new client.Gauge({
        name: `ping_time`,
        help: 'Internet Ping Duration',
        registers: [registry]
    });
    const download = new client.Gauge({
        name: 'download_speed',
        help: 'Internet Download Speed',
        registers: [registry]
    });
    const upload = new client.Gauge({
        name: 'upload_speed',
        help: 'Internet Upload Speed',
        registers: [registry]
    });
    ping.set(result.ping);
    upload.set(result.uploadSpeed);
    download.set(result.downloadSpeed);

    return gateway.pushAdd({ jobName: JOBNAME });
});